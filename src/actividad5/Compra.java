package actividad5;

import actividad5.articulo.Produccion;

import java.util.Objects;

public class Compra {

    private Produccion produccion;

    private Date fechaDeVenta;

    private String comentario;

    public Compra(Produccion produccion, Date fechaDeVenta) {
        this.produccion = produccion;
        this.fechaDeVenta = fechaDeVenta;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public float getPrecio() {
        return produccion.getPrize();
    }

    @Override
    public String toString() {
        String texto = produccion.getTitle() + " - fecha: " + fechaDeVenta + " ";
        if (comentario != null) {
            texto += "- comentario: " + comentario;
        } else {
            texto += "- sin comentario";
        }
        return texto;
    }

    public Produccion getProduccion() {
        return produccion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Compra compra)) { return false; };
        return Objects.equals(produccion, compra.produccion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(produccion);
    }
}
