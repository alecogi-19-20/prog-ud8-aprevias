package actividad5.articulo;

import actividad5.Date;

public class Serie extends Produccion {

    private int season;

    private int chapter;

    public Serie(String title, long duration, Format format, Date releaseDate, int season, int chapter) {
        super(title, duration, format, releaseDate, 24f);
        this.season = season;
        this.chapter = chapter;
    }

    public void showDetails() {
        System.out.println("-------------------Serie--------------------");
        System.out.printf("%s (%d)%n",  getTitle(), releaseDate.getAnyo());
        System.out.printf("Capitulo: %d - Temporada %d %n", chapter, season);
        System.out.println("Descripcion: " + getSummary());
        System.out.println("Duracion: " + getDuration());
        System.out.println("--------------------------------------------");
    }

    @Override
    public String toString() {
        return super.toString() + " (Serie) temporada = " + season + ", capítulo = " + chapter;
    }
}
