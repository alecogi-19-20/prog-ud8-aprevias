package actividad5.articulo;

import actividad5.Date;

import java.util.Objects;

public abstract class Produccion {

    public enum Format { MPG, AVI, MOV, FLV };

    private String title;

    private long duration;

    private String summary;

    private Format format;

    protected Date releaseDate;

    private float prize;

    public Produccion(String title, long duration, Format format, Date releaseDate, float prize) {
        this.title = title;
        this.duration = duration;
        this.format = format;
        this.releaseDate = releaseDate;
        this.prize = prize;
    }

    public void showDetails() {
        System.out.println("Not implemented");
    }

    public String getTitle() {
        return title;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummary() {
        return summary;
    }

    public long getDuration() {
        return duration;
    }

    public float getPrize() {
        return prize;
    }

    @Override
    public String toString() {
        return "Producción: " + "titulo = " + title + ", duración=" + duration
                + ", formato=" + format;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Produccion that)) { return false; };
        return title.equals(that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}
