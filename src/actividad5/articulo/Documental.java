package actividad5.articulo;

import actividad5.Date;

public class Documental extends Produccion {

   private String investigador;

   private String tema;

    public Documental(String title, long duration, Format format, Date releaseDate, String investigador, String tema) {
        super(title, duration, format, releaseDate, 15f);
        this.investigador = investigador;
        this.tema = tema;
    }

    @Override
    public void showDetails() {
        System.out.println("---------------- Documental ---------------");
        System.out.printf("%s (%d)%n",  getTitle(), releaseDate.getAnyo());
        System.out.println("Descripcion: " + getSummary());
        System.out.println("Investigador: " + investigador);
        System.out.println("Duracion: " + getDuration());
        System.out.println("--------------------------------------------");
    }

    @Override
    public String toString() {
        return super.toString() + " (documental)" + " investigador: = " + investigador + ", tema= " + tema;
    }

}
