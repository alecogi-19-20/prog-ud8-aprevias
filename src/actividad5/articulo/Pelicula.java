package actividad5.articulo;

import actividad5.Date;

public class Pelicula extends Produccion {

    private String mainActor;

    private String mainActress;

    public Pelicula(String title, long duration, Format format, Date releaseDate, String mainActor) {
        super(title, duration, format, releaseDate, 12f);
        this.mainActor = mainActor;
    }

    public Pelicula(String title, long duration, Format format, Date releaseDate,
                    String mainActor, String mainActress) {
        super(title, duration, format, releaseDate, 12f);
        this.mainActor = mainActor;
        this.mainActress = mainActress;
    }

    @Override
    public void showDetails() {
        System.out.println("----------------- Película ----------------");
        System.out.printf("%s (%d)%n",  getTitle(), releaseDate.getAnyo());
        System.out.println("Descripcion: " + getSummary());
        System.out.println("Duracion: " + getDuration());
        System.out.println("--------------------------------------------");
    }

    @Override
    public String toString() {
        return super.toString() + " (pelicula)" + " actor: = " + mainActor + ", actriz= " + mainActress;
    }
}
