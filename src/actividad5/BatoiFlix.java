package actividad5;

import actividad5.articulo.Documental;
import actividad5.articulo.Pelicula;
import actividad5.articulo.Produccion;
import actividad5.articulo.Serie;

public class BatoiFlix {

    public static void main(String[] args) {

        Date releaseDate = new Date("19/02/2021");
        Documental dreamSongs = new Documental("Dream Songs",
                2400L, Produccion.Format.MPG, releaseDate, "Enrique Juncosa",
                "Movimiento Hippie");
        dreamSongs.setSummary("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        releaseDate = new Date("20/06/2020");
        Pelicula newsOfTheWorld = new Pelicula("News of the World",
                7200L, Produccion.Format.AVI, releaseDate, "Tom Hanks",
                "Carolina Betller");
        newsOfTheWorld.setSummary("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        Date date = new Date("04/06/2001");
        Serie elHacker = new Serie("El hacker",
                3600L, Produccion.Format.FLV, date,
                1, 20);
        elHacker.setSummary("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        Cliente alex = new Cliente("Juan", "Perez", "juan@batoiflix.com");
        Cliente maria = new Cliente("Maria", "Garcia", "maria@batoiflix.com");
        Cliente elena = new Cliente("Elena", "Ramírez", "elena@batoiflix.com");

        Date fechaCompra = new Date("04/03/2021");
        Compra compra1 = new Compra(newsOfTheWorld, fechaCompra);

        Date fechaCompra2 = new Date("05/03/2021");
        Compra compra2 = new Compra(elHacker, fechaCompra2);
        Compra compra3 = new Compra(dreamSongs, fechaCompra2);

        maria.comprar(compra1);
        maria.comprar(compra2);
        maria.comprar(compra3);

        Compra compra4 = new Compra(dreamSongs, fechaCompra2);
        alex.comprar(compra4);
        alex.setComentario(dreamSongs, "Es una pasada de película");

        Compra compra5 = new Compra(newsOfTheWorld, fechaCompra2);
        elena.comprar(compra5);

        System.out.println(maria);
        System.out.println(alex);
        System.out.println(elena);

    }

}