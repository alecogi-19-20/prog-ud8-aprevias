package actividad5;

import actividad5.articulo.Produccion;

import java.util.ArrayList;

public class Cliente {

    private ArrayList<Compra> compras;

    private String nombre;

    private String apellidos;

    private String email;

    public Cliente(String nombre, String apellidos, String email) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.compras = new ArrayList<>();
    }

    public void comprar(Compra compra) {
        if (!compras.contains(compra)) {
            compras.add(compra);
        }
    }

    public boolean setComentario(Produccion pelicula, String comentario){
        for (Compra compra: compras) {
            if (compra.getProduccion().equals(pelicula)) {
                compra.setComentario(comentario);
                return true;
            }
        }
        return false;
    }

    public float getTotalGastado() {
        float totalGastado = 0;
        for (Compra compra: compras) {
            totalGastado += compra.getPrecio();
        }
        return totalGastado;
    }
    @Override
    public String toString() {
        return String.format("Cliente: %s %s | Compras(%d): %s - Total gastado: %.2f€", nombre, apellidos,
                compras.size(), compras, getTotalGastado());
    }
}
