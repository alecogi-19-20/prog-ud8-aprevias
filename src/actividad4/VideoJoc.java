package actividad4;

/**
 * ctivitat 4.- Crea una classe anomenada VideoJoc. Per a cada videojoc, emmagatzemarem el seu títol, gènere, preu i si és multijugador o no.
 * Seguidament crea una clase TestVideoClub on declares un ArrayList amb el següent llistat de videojocs i seguidament esciu un programa que pregunte a l'usuari pel nom d’un videojoc que vullga llogar e indiqueu per pantalla si se'n disposa o no d’ell.
 * Nota. Hauràs de sobreescriure el mètode equals.
 */
public class VideoJoc {

    public enum Genero {
        ACCION, DEPORTES, SIMULACION
    }

    private Genero genero;

    private String titulo;

    private float precio;

    private boolean multijugador;

    public VideoJoc(Genero genero, String titulo, float precio, boolean multijugador) {
        this.genero = genero;
        this.titulo = titulo;
        this.precio = precio;
        this.multijugador = multijugador;
    }

    public VideoJoc(Genero genero, String titulo, float precio) {
        this(genero, titulo, precio, true);
    }

    public VideoJoc(String titulo) {
        this(null, titulo, 0f, true);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof VideoJoc)) {
            return false;
        }
        return ((VideoJoc) obj).titulo.equalsIgnoreCase(titulo);
    }
}
