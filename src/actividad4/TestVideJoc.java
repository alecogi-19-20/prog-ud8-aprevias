package actividad4;

import java.util.ArrayList;
import java.util.Scanner;

public class TestVideJoc {

    private final static Scanner teclat = new Scanner(System.in);

    public static void main(String[] args) {

        ArrayList<VideoJoc> llistat = new ArrayList<>();

        llistat.add(new VideoJoc(VideoJoc.Genero.ACCION,"Fortnite", 40f));
        llistat.add(new VideoJoc(VideoJoc.Genero.DEPORTES,"Fifa", 50f));
        llistat.add(new VideoJoc(VideoJoc.Genero.ACCION,"Gran Theft Auto", 80f));
        llistat.add(new VideoJoc(VideoJoc.Genero.SIMULACION,"Minecraft", 60f));
        llistat.add(new VideoJoc(VideoJoc.Genero.SIMULACION,"Anima Crossing", 30f));

        String titolVideojoc = demanarNom();
        VideoJoc videoJocABuscar = new VideoJoc(titolVideojoc);

        if (llistat.contains(videoJocABuscar)) {
            System.out.println("Es alquilable");
        } else {
            System.out.println("No está disponible");
        }

    }

    private static String demanarNom() {
        System.out.println("Introduce el títol del videjoc que vol alquilar");
        return teclat.next();
    }
}
