import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class Actividad1 {

    private final static Scanner teclat = new Scanner(System.in);
    public static void main(String[] args) {

        ArrayList<String> listado = new ArrayList<>();
        listado.add("rojo");
        listado.add("naranja");
        listado.add("amarillo");
        listado.add("verde");
        listado.add("añil");
        listado.add("azul");
        listado.add("violeta");

        System.out.println(listado.size());
        System.out.println(listado.indexOf("rojo"));

        for (String item: listado) {
            System.out.println(item);
        }

        for (int i = 0; i < listado.size() ; i++) {
            System.out.println(listado.get(i));
        }

        demanarNouColor(listado);

    }

    /**
     * Crea un mètode que demani a l'usuari que insereixa un color e indique si aquest color es troba a la llista,
     * en cas contrari haureu d'afegir aquest color al llistat.
     * @return
     */
    private static void demanarNouColor(ArrayList<String> listado) {
        System.out.println("Introdueix un color");
        String nouColor = teclat.next();
        if (listado.contains(nouColor)) {
            System.out.println("El color ya existe");
        } else {
            listado.add(nouColor);
            System.out.println("El color ha sido añadido con éxito");
        }
    }
}