import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Actividad2 {

    public static Scanner teclat = new Scanner(System.in);

    public static void main(String[] args) {

        ArrayList<String> listadoAlumnos = new ArrayList<>();
        listadoAlumnos.add("Maria");
        listadoAlumnos.add("Juan");
        listadoAlumnos.add("Ernesto");
        listadoAlumnos.add("Francisco");
        listadoAlumnos.add("Núria");
        listadoAlumnos.add("Cintia");

        Iterator<String> listIterator = listadoAlumnos.iterator();

        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
        }

        String nombre = demanarNom();
        if (listadoAlumnos.contains(nombre)) {
            System.out.println("Es compañero");
        } else {
            System.out.println("No es compañero");
        }

    }

    private static String demanarNom() {
        System.out.println("Introduce un nombre");
        return teclat.next();
    }
}
