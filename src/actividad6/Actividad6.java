package actividad6;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Activitat 6. Crea un programa que demane a l'usuari noms de persones
 * fins que introduïsca la cadena “FIN”. En acabar hauràs de mostrar
 * tots els noms d’usuari (sense repeticions). Utilitza una estructura HashSet
 * Nota: Els noms de persona es sol·licitaran a una única
 * línia separats per espais en blanc.
 */
public class Actividad6 {

    public static final String CONTROL_FIN = "FIN";

    public static void main(String[] args) {
        HashSet<String> llistatDeNoms = demanarNoms();
        System.out.println(llistatDeNoms);
    }

    public static HashSet<String> demanarNoms() {
        System.out.print("Introdueix noms de persones separats per espais: ");
        Scanner teclado = new Scanner(System.in);
        HashSet<String> llistatNoRepetit = new HashSet<>();
        do {
            String[] noms = teclado.nextLine().trim().split(" ");
            for (String nom: noms) {
                if (nom.equals(CONTROL_FIN)) {
                    return llistatNoRepetit;
                }
                llistatNoRepetit.add(nom);
            }
            System.out.print("Introdueix noms de persones: ");
        } while (true);
    }

}
