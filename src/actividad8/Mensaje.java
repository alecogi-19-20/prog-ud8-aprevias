package actividad8;

import java.time.LocalDateTime;

public class Mensaje {

    private String descripcion;

    private LocalDateTime fechaCreacion;

    public Mensaje(String descripcion) {
        this.descripcion = descripcion;
        this.fechaCreacion = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return fechaCreacion + " - " + descripcion;
    }

}
