package actividad8;

import java.util.LinkedList;

public class TestForo {

    public static void main(String[] args) {

        LinkedList<Foro> foros = new LinkedList<>();

        Foro basica1 = new Foro("basica 1");
        foros.add(basica1);

        ForoInteligente inteligente1 = new ForoInteligente("inteligente 1", "facebook", "thepiratebay");
        foros.add(inteligente1);

        ForoInteligente inteligente2 = new ForoInteligente("inteligente 2", "foto", "descargar");
        foros.add(inteligente2);

        for (Foro bitacora: foros) {
            bitacora.registrarEntrada("Se ha producido un error en el servicio");
            bitacora.registrarEntrada("Puedes descargar el driver desde thepiratebay");
            bitacora.registrarEntrada("Las fotos están publicadas en facebook");
        }

        for (Foro bitacora: foros) {
            System.out.println(bitacora);
        }

    }

}
