package actividad8;

import java.util.ArrayList;

public class Foro {

    private String nombre;

    private ArrayList<Mensaje> entradas;

    public Foro(String nombre) {
        entradas = new ArrayList<>();
        this.nombre = nombre;
    }

    public boolean registrarEntrada(String descripcion) {
        return this.entradas.add(new Mensaje(descripcion));
    }

    @Override
    public String toString() {
        StringBuilder entradaBuilder = new StringBuilder();
        for (Mensaje entrada: entradas) {
            entradaBuilder.append("\n" + entrada);
        }
        return String.format("----- %s (%d) %s ------ %s \n", getClass().getSimpleName(),
                entradas.size(), nombre, entradaBuilder);
    }
}
