package actividad8;

import java.util.HashSet;
import java.util.List;

public class ForoInteligente extends Foro {

    private HashSet<String> palabrasProhibidas;

    private HashSet<String> listaSpam;

    public ForoInteligente(String nombre, String... palabrasProhibidas) {
        super(nombre);
        this.listaSpam = new HashSet<>();
        this.palabrasProhibidas = new HashSet<>(List.of(palabrasProhibidas));
    }

    @Override
    public boolean registrarEntrada(String descripcion) {
        String[] palabras = descripcion.split(" ");
        for (String palabra: palabras) {
            if (palabrasProhibidas.contains(palabra)) {
                this.listaSpam.add(descripcion);
                return false;
            }
        }
        return super.registrarEntrada(descripcion);
    }

    @Override
    public String toString() {
        return String.format("%sSucesos spam: %d\n", super.toString(), listaSpam.size());
    }

}
