package actividad7;

import java.util.Objects;

public class Participante {
    private String nombre;
    private String dni;
    private float tiempo;

    public Participante(String nombre, String dni, float tiempo) {
        this.nombre = nombre;
        this.dni = dni;
        this.tiempo = tiempo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participante that = (Participante) o;
        return dni.equals(that.dni);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dni);
    }

    @Override
    public String toString() {
        return "nombre:" + nombre
                + " dni: " + dni;
    }
}

