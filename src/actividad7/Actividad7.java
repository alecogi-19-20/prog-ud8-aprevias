package actividad7;

import java.util.HashSet;

public class Actividad7 {
    public static void main(String[] args) {
        HashSet<Participante> participantesPrueba1 = new HashSet<>();
        participantesPrueba1.add(new Participante("Toni García", "12345678A", 12.5f));
        participantesPrueba1.add(new Participante("Pepito García", "52523678A", 21.5f));

        HashSet<Participante> participantesPrueba2 = new HashSet<>();
        participantesPrueba2.add(new Participante("Toñi García", "12345678A", 12.5f));
        participantesPrueba2.add(new Participante("Ana Montes", "525212318A", 31.5f));

        HashSet<Participante> participantesUnicos = new HashSet<>();
        participantesUnicos.addAll(participantesPrueba1);
        participantesUnicos.addAll(participantesPrueba2);

        System.out.println("Prueba 1");
        System.out.println(participantesPrueba1);

        System.out.println("Prueba 2");
        System.out.println(participantesPrueba2);

        System.out.println("Únicos");
        System.out.println(participantesUnicos);
    }


}
