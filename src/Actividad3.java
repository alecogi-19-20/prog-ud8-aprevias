import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Activitat 3.- Realitza un programa que introduïsca 50 valors aleatoris (entre 0 i 100)
 * en un ArrayList i que després calcule la suma, la mitjana, el màxim
 * i el mínim d 'aquests números.
 */
public class Actividad3 {

    public static final int TOTAL_ELEMENTS = 20;

    public static void main(String[] args) {

        ArrayList<Integer> listadoAleatorios = getLlistatAleatoris(TOTAL_ELEMENTS);
        System.out.println(listadoAleatorios);
        int totalSuma = 0;
        for (int numero: listadoAleatorios) {
            totalSuma += numero;
        }
        System.out.println("Total suma: " + totalSuma);
        Collections.sort(listadoAleatorios);
        System.out.println(listadoAleatorios);
        System.out.printf("La mediana es %d %n", listadoAleatorios.get(listadoAleatorios.size() / 2));
        System.out.printf("El máxim es %d %n", listadoAleatorios.get(listadoAleatorios.size() - 1));
        System.out.printf("El mínim es %d %n", listadoAleatorios.get(0));

    }

    public static ArrayList<Integer> getLlistatAleatoris(int num) {
        ArrayList<Integer> listado = new ArrayList<>();
        Random generadorAleatorios = new Random();
        for (int i = 0; i < num; i++) {
            int numAleatorio = generadorAleatorios.nextInt(0, 101);
            listado.add(numAleatorio);
        }
        return listado;
    }
}
